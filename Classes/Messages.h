#pragma once

#include <map>
#include <string>

using namespace std;

class Messages {
public:
	static Messages* getInstance();

	void init();

	string getMenuText(string key);

private:
	Messages();

	map<string, string> _menu;

	string getTextFromMap(map<string, string>& map, string& key);

	void loadFileToMap(const string& fileName, map<string, string>& map);
};