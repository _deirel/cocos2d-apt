#include "LenSprite.h"

USING_NS_CC;

bool LenSprite::init() {
	if (!Node::init()) return false;

	auto frame = Sprite::create("lens/len.png", Rect(0, 0, 16, 22));
	auto glass = Sprite::create("lens/len.png", Rect(16, 0, 16, 22));
	frame->getTexture()->setAliasTexParameters();
	glass->getTexture()->setAliasTexParameters();
	glass->setBlendFunc(BlendFunc::ADDITIVE);
	glass->setName("glass");
	addChild(glass);
	addChild(frame);
	setContentSize(frame->getContentSize());
	setScale(4.0f);
	addEvents();
	return true;
}

void LenSprite::setLen(Len * len) {
	getChildByName("glass")->setColor(len->getColor());
	_lenId = len->getId();
}

void LenSprite::moveTo(const Vec2 & pos) {
	performMove(pos);
}

void LenSprite::moveBack() {
	performMove(_defaultPosition);
}

void LenSprite::addEvents() {
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = [&](Touch* touch, Event* event) {
		auto touchLocation = touch->getLocation();
		auto rect = utils::getCascadeBoundingBox(this);
		if (rect.containsPoint(touchLocation)) {
			if (onClick) onClick(this);
			return true;
		}
		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void LenSprite::performMove(const cocos2d::Vec2 & pos) {
	auto move = MoveTo::create(0.5f, pos);
	auto cb = CallFunc::create([&]() { 
		_eventDispatcher->resumeEventListenersForTarget(this);
		if (onEndMoving) onEndMoving(this); 
	});
	_eventDispatcher->pauseEventListenersForTarget(this);
	runAction(Sequence::create(move, cb, nullptr));
}
