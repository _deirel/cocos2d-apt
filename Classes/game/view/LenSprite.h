#pragma once

#include "cocos2d.h"
#include "..\model\Len.h"
#include "ui\CocosGUI.h"

class LenSprite : public cocos2d::Node {
public:
	CREATE_FUNC(LenSprite);
	
	bool init();

	int getLenId() { return _lenId; }

	void setLen(Len* len);

	void setDefaultPosition(const cocos2d::Vec2& pos, bool move = true) { 
		_defaultPosition = pos; 
		if (move) setPosition(_defaultPosition);
	}

	void moveTo(const cocos2d::Vec2& pos);

	void moveBack();

	std::function<void(LenSprite*)> onClick;
	std::function<void(LenSprite*)> onEndMoving;

private:
	LenSprite() {}

	void performMove(const cocos2d::Vec2& pos);

	void addEvents();

	int _lenId;

	cocos2d::Vec2 _defaultPosition;
};