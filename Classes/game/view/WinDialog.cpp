#include "WinDialog.h"
#include "..\..\Fonts.h"
#include "..\..\scenes\GameScene.h"
#include "..\..\scenes\MenuScene.h"
#include "Messages.h"

USING_NS_CC;

bool WinDialog::init() {
	if (!Node::init()) return false;

	auto winSize = Director::getInstance()->getWinSizeInPixels();

	auto fade = DrawNode::create(0);
	fade->drawSolidRect(Vec2(0.0f, 0.0f), Vec2(winSize.width, winSize.height), Color4F(0.0f, 0.0f, 0.0f, 0.6f));
	addChild(fade);

	auto winLabel = Label::createWithTTF("YOU WIN!", "fonts/main.ttf", 30.0f);
	winLabel->setPosition(Vec2(0.0f, 120.0f) + winSize * 0.5f);
	addChild(winLabel);

	Vector<MenuItem*> menuItems;
	menuItems.pushBack(createMenuItem("restart", CC_CALLBACK_1(WinDialog::menuRestartCallback, this), 0.0f));
	menuItems.pushBack(createMenuItem("main_menu", CC_CALLBACK_1(WinDialog::menuMainMenuCallback, this), -40.0f));
	menuItems.pushBack(createMenuItem("exit", CC_CALLBACK_1(WinDialog::menuExitCallback, this), -80.0f));

	auto menu = Menu::createWithArray(menuItems);
	menu->setPosition(Vec2(0.0f, 40.0f) + winSize * 0.5f);
	addChild(menu);

	return true;
}

MenuItem* WinDialog::createMenuItem(const char* key, ccMenuCallback callback, float y) {
	auto label = Label::createWithTTF(Messages::getInstance()->getMenuText(key), Fonts::MAIN, 20.0f);
	auto item = MenuItemLabel::create(label, callback);
	item->setPosition(Vec2(0.0f, y));
	return item;
}

void WinDialog::menuRestartCallback(cocos2d::Ref * sender) {
	Director::getInstance()->replaceScene(TransitionFade::create(0.4f, GameScene::create()));
}

void WinDialog::menuMainMenuCallback(cocos2d::Ref * sender) {
	Director::getInstance()->replaceScene(TransitionFade::create(0.4f, MenuScene::create()));
}

void WinDialog::menuExitCallback(cocos2d::Ref * sender) {
	Director::getInstance()->end();
}
