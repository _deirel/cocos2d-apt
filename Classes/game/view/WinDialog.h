#pragma once

#include "cocos2d.h"

class WinDialog : public cocos2d::Node {
public:
	CREATE_FUNC(WinDialog);

	bool init();

private:
	cocos2d::MenuItem* createMenuItem(const char* key, cocos2d::ccMenuCallback callback, float y);

	void menuRestartCallback(cocos2d::Ref* sender);
	void menuMainMenuCallback(cocos2d::Ref* sender);
	void menuExitCallback(cocos2d::Ref* sender);
};