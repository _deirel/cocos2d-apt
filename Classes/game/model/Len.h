#pragma once

#include "cocos2d.h"

enum class LenState {
	NON_ACTIVE, TRANSITION, ACTIVE
};

class Len  {
public:
	Len(cocos2d::Color3B color) : _state(LenState::NON_ACTIVE), _stateTo(LenState::NON_ACTIVE), _color(color), _id(++_idCounter) {}

	LenState getState() { return _state; }
	LenState getStateTo() { return _stateTo; }

	int getId() { return _id; }

	cocos2d::Color3B getColor() { return _color; }

	std::function<void(Len*)> onToggleBegin;
	std::function<void(Len*)> onToggled;

	void beginToggle() {
		_state = LenState::TRANSITION;
		_stateTo = _stateTo == LenState::ACTIVE ? LenState::NON_ACTIVE : LenState::ACTIVE;
		if (onToggleBegin) onToggleBegin(this);
	}

	void finishToggle() {
		_state = _stateTo;
		if (onToggled) onToggled(this);
	}

private:
	static int _idCounter;

	LenState _state;

	LenState _stateTo;

	int _id;

	cocos2d::Color3B _color;
};