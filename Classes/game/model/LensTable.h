#pragma once

#include "Len.h"

class LensTable {
public:
	std::function<void()> onWin;
	std::function<void(Len*)> onLenToggleBegin;
	std::function<void(Len*)> onLenToggled;

	void addLen(Len* len);

	void beginToggleLen(int id);
	void finishToggleLen(int id);
	void checkWin();

private:
	std::map<int, std::shared_ptr<Len>> _lens;
};