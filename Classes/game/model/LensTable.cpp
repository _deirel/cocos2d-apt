#include "LensTable.h"

void LensTable::addLen(Len* len) {
	_lens[len->getId()] = std::shared_ptr<Len>(len);
	
	len->onToggleBegin = [&](Len* len) {
		if (onLenToggleBegin) onLenToggleBegin(len);
	};

	len->onToggled = [&](Len* len) {
		if (onLenToggled) onLenToggled(len);
	};
}

void LensTable::beginToggleLen(int id) {
	auto pair = _lens.find(id);
	if (pair != _lens.end()) pair->second->beginToggle();
}

void LensTable::finishToggleLen(int id) {
	auto pair = _lens.find(id);
	if (pair != _lens.end()) pair->second->finishToggle();
}

void LensTable::checkWin() {
	bool win = true;
	for (auto len : _lens) {
		win = win && len.second->getState() == LenState::ACTIVE;
		if (!win) break;
	}
	if (win && onWin) onWin();
}