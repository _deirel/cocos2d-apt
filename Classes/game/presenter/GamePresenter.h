#pragma once

#include "cocos2d.h"
#include "..\model\LensTable.h"
#include "..\view\LenSprite.h"

class GamePresenter {
public:
	GamePresenter(cocos2d::Scene* scene);

private:
	void createTable();
	void createLens();

	LenSprite* createLen(cocos2d::Color3B color);

	void onLenClick(LenSprite* sender);
	void onLenEndMoving(LenSprite* sender);
	
	void onLenBeginToggle(Len *len);
	void onLenToggled(Len *len);
	void onWin();

	void pause();
	
	LenSprite* getSprite(int lenId);

	std::map<int, LenSprite*> _lenSprites;

	std::shared_ptr<LensTable> _table;

	cocos2d::Scene* _scene;
	cocos2d::Node* _handler;
};