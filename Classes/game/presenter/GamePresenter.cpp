#include "GamePresenter.h"
#include <random>
#include "SimpleAudioEngine.h"
#include "..\view\WinDialog.h"

USING_NS_CC;

GamePresenter::GamePresenter(Scene* scene) : _scene(scene) {
	createTable();

	_handler = Node::create();
	_scene->addChild(_handler);

	createLens();
}

void GamePresenter::createTable() {
	_table = std::make_shared<LensTable>();
	_table->onLenToggleBegin = CC_CALLBACK_1(GamePresenter::onLenBeginToggle, this);
	_table->onLenToggled = CC_CALLBACK_1(GamePresenter::onLenToggled, this);
	_table->onWin = CC_CALLBACK_0(GamePresenter::onWin, this);
}

void GamePresenter::createLens() {
	auto winSize = Director::getInstance()->getWinSizeInPixels();

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(0, 255);

	auto color1 = Color3B(dist(mt), dist(mt), dist(mt));
	auto color2 = Color3B(dist(mt), dist(mt), dist(mt));
	auto color3 = Color3B(255 - color1.r - color2.r, 255 - color1.g - color2.g, 255 - color1.b - color2.b);

	auto spr = createLen(color1);
	spr->setDefaultPosition(Vec2(spr->getBoundingBox().size.width / 2.0f, winSize.height / 2.0f));

	spr = createLen(color2);
	spr->setDefaultPosition(Vec2(winSize.width / 2.0f, winSize.height - spr->getBoundingBox().size.height / 2.0f));

	spr = createLen(color3);
	spr->setDefaultPosition(Vec2(winSize.width - spr->getBoundingBox().size.width / 2.0f, winSize.height / 2.0f));
}

LenSprite * GamePresenter::createLen(cocos2d::Color3B color) {
	auto len = new Len(color);
	_table->addLen(len);
	
	auto lenSprite = LenSprite::create();
	lenSprite->setLen(len);
	lenSprite->onClick = CC_CALLBACK_1(GamePresenter::onLenClick, this);
	lenSprite->onEndMoving = CC_CALLBACK_1(GamePresenter::onLenEndMoving, this);
	
	_handler->addChild(lenSprite);
	_lenSprites[len->getId()] = lenSprite;

	return lenSprite;
}

void GamePresenter::onLenClick(LenSprite* sender) {
	for (auto pair : _lenSprites) pair.second->setLocalZOrder(pair.second == sender ? 1 : 0);
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("snd/click.mp3");
	_table->beginToggleLen(sender->getLenId());
}

void GamePresenter::onLenEndMoving(LenSprite * sender) {
	_table->finishToggleLen(sender->getLenId());
}

void GamePresenter::onLenBeginToggle(Len * len) {
	auto lenSprite = getSprite(len->getId());
	if (lenSprite != nullptr) {
		LenState stateTo = len->getStateTo();
		if (stateTo == LenState::NON_ACTIVE) lenSprite->moveBack();
		else if (stateTo == LenState::ACTIVE) lenSprite->moveTo(Director::getInstance()->getWinSizeInPixels() * 0.5f);
	}
}

void GamePresenter::onLenToggled(Len * len) {
	_table->checkWin();
}

void GamePresenter::onWin() {
	pause();

	auto face = Sprite::create("lens/face.png");
	face->setPosition(Vec2(0.0f, -4.0f) + Director::getInstance()->getWinSizeInPixels() * 0.5f);
	face->setScale(4.0f);
	face->getTexture()->setAliasTexParameters();
	_handler->addChild(face, 2);

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("snd/win.mp3");

	auto delay = DelayTime::create(2.0f);
	auto showWinDialog = CallFunc::create([&]() {
		auto winDialog = WinDialog::create();
		winDialog->setCascadeOpacityEnabled(true);
		winDialog->setOpacity(0);
		auto fade = FadeIn::create(0.3f);
		_scene->addChild(winDialog);
		winDialog->runAction(fade);
	});
	
	_scene->runAction(Sequence::create(delay, showWinDialog, nullptr));
}

void GamePresenter::pause() {
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(_handler, true);
}

LenSprite * GamePresenter::getSprite(int lenId) {
	auto pair = _lenSprites.find(lenId);
	return pair != _lenSprites.end() ? pair->second : nullptr;
}
