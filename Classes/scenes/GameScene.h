#pragma once

#include "cocos2d.h"
#include "..\game\presenter\GamePresenter.h"

class GameScene : public cocos2d::Scene {
public:
	void onEnter() override;

	CREATE_FUNC(GameScene);

private:
	std::shared_ptr<GamePresenter> _game;
};

