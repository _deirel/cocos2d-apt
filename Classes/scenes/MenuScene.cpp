#include "MenuScene.h"
#include "GameScene.h"
#include "..\Fonts.h"

#include "Messages.h"

USING_NS_CC;

bool MenuScene::init() {
	if (!Scene::init()) return false;

	Vector<MenuItem*> menuItems;
	menuItems.pushBack(createMenuItem("start", CC_CALLBACK_1(MenuScene::menuStartCallback, this), 0.0f));
	menuItems.pushBack(createMenuItem("exit", CC_CALLBACK_1(MenuScene::menuExitCallback, this), -40.0f));

	auto menu = Menu::createWithArray(menuItems);
	menu->setPosition(Vec2(0.0f, 20.0f) + Director::getInstance()->getWinSizeInPixels() * 0.5f);
	addChild(menu);

	return true;
}

MenuItem* MenuScene::createMenuItem(const char* key, ccMenuCallback callback, float y) {
	auto label = Label::createWithTTF(Messages::getInstance()->getMenuText(key), Fonts::MAIN, 20.0f);
	auto item = MenuItemLabel::create(label, callback);
	item->setPosition(Vec2(0.0f, y));
	return item;
}

void MenuScene::menuExitCallback(cocos2d::Ref* sender) {
	Director::getInstance()->end();
	// ����� ����������� ��� ��� iOS
}

void MenuScene::menuStartCallback(cocos2d::Ref * sender) {
	Director::getInstance()->replaceScene(TransitionFade::create(0.4f, GameScene::create()));
	
}


