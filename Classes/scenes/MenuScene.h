#pragma once

#include <functional>
#include "cocos2d.h"

class MenuScene : public cocos2d::Scene {
public:
	virtual bool init();

	void menuStartCallback(cocos2d::Ref* sender);
	void menuExitCallback(cocos2d::Ref* sender);

	CREATE_FUNC(MenuScene);
private:
	cocos2d::MenuItem* createMenuItem(const char* key, cocos2d::ccMenuCallback callback, float y);
};