#pragma once

#include "cocos2d.h"

class PreloaderScene : public cocos2d::Scene {
public:
	CREATE_FUNC(PreloaderScene);

	bool init();

	void onEnter();

	void loadResources();
};