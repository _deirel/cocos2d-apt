#include "PreloaderScene.h"
#include "SimpleAudioEngine.h"
#include "Messages.h"
#include "MenuScene.h"

bool PreloaderScene::init() {
	if (!Scene::init()) return false;

	auto logo = cocos2d::Sprite::create("logo.png");
	auto winSize = cocos2d::Director::getInstance()->getWinSizeInPixels();
	logo->setPosition(winSize * 0.5f);
	logo->getTexture()->setAliasTexParameters();
	logo->setScale(4.0f);
	logo->setOpacity(0);
	logo->setName("logo");
	addChild(logo);

	return true;
}

void PreloaderScene::onEnter() {
	cocos2d::Scene::onEnter();

	auto fade = cocos2d::FadeIn::create(0.6f);
	auto fadeCb = cocos2d::CallFunc::create(CC_CALLBACK_0(PreloaderScene::loadResources, this));
	auto logo = getChildByName("logo");
	
	logo->runAction(cocos2d::Sequence::create(fade, fadeCb, nullptr));
}

void PreloaderScene::loadResources() {
	// ������������ ��������
	// �����
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("snd/click.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("snd/win.mp3");

	// �����
	Messages::getInstance()->init();

	cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(0.4f, MenuScene::create()));
}
