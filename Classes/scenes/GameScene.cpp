#include "GameScene.h"

USING_NS_CC;

void GameScene::onEnter() {
	Scene::onEnter();
	_game = std::make_shared<GamePresenter>(this);
}
