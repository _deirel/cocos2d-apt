#include "Messages.h"
#include <sstream>
#include "json\document.h"
#include "cocos2d.h"

USING_NS_CC;

Messages * Messages::getInstance() {
	static Messages instance;
	return &instance;
}

void Messages::init() {
	loadFileToMap("menu.json", _menu);
}

string Messages::getMenuText(string key) {
	return getTextFromMap(_menu, key);
}

Messages::Messages() {
}

string Messages::getTextFromMap(map<string, string>& map, string & key) {
	auto pair = map.find(key);
	if (pair != map.end()) return pair->second;
	ostringstream s;
	s << "<" << key << ":no-value>";
	return s.str();
}

void Messages::loadFileToMap(const string & fileName, map<string, string>& map) {
	auto menuStr = FileUtils::getInstance()->getStringFromFile(fileName);
	rapidjson::Document document;
	document.Parse(menuStr.c_str());
	for (auto it = document.MemberBegin(); it != document.MemberEnd(); ++it) {
		map.insert(pair<string, string>(it->name.GetString(), it->value.GetString()));
	}
}
